import express, { Request, Response, NextFunction } from "express";
import "express-async-errors";
import connection from "./database";
import routes from "./routes";
import { ErrorApp } from "./middlewares/error";

connection();

const app = express();

app.use(express.json());
app.use(routes);

app.use((err: Error, req: Request, res: Response, next: NextFunction) =>
  ErrorApp(err, req, res, next)
);

export default app;
