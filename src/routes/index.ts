import { Router } from "express";
import userRouter from "./user.routes";
import sessionRouter from "./login.routes";

const routes = Router();

routes.use("/users", userRouter);
routes.use("/login", sessionRouter);

export default routes;
