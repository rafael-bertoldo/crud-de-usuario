import { Router } from "express";
import AuthService from "../services/AuthService";
import { classToClass } from "class-transformer";

const sessionRouter = Router();

sessionRouter.post("/", async (req, res) => {
  const { email, password } = req.body;

  const authUser = new AuthService();

  const userResponse = await authUser.execute({
    email,
    password,
  });

  return res.json(classToClass(userResponse));
});

export default sessionRouter;
