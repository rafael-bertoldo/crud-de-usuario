import { classToClass } from "class-transformer";
import { Router } from "express";
import {
  createUser,
  deleteUser,
  listExpecificUser,
  listUser,
  updateUser,
} from "../controllers/user.controller";
import authentication from "../middlewares/authentication";
import isAdm from "../middlewares/isAdm";
import validator from "../middlewares/validator";

const userRouter = Router();

userRouter.post("/", async (req, res) => {
  const { name, email, password, isAdm } = req.body;
  const user = await createUser(name, email, password, isAdm, res);

  res.status(201).json(user);
});

userRouter.use(authentication);

userRouter.get("/", isAdm, async (req, res) => {
  const user = await listUser(res);

  res.status(200).json(classToClass(user));
});

userRouter.get("/profile", async (req, res) => {
  const userId = req.user.id;
  const user = await listExpecificUser(userId, res);

  res.status(200).json(classToClass(user));
});

userRouter.patch("/:uuid", validator, async (req, res) => {
  const { uuid } = req.params;
  const { name, email } = req.body;

  const user = await updateUser(name, email, uuid);

  res.json(classToClass(user));
});

userRouter.delete("/:uuid", isAdm, async (req, res) => {
  const { uuid } = req.params;

  await deleteUser(uuid);

  return res.status(204).json();
});

export default userRouter;
