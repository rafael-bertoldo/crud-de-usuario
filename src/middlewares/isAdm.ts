import { Request, Response, NextFunction } from "express";
import { listExpecificUser } from "../controllers/user.controller";
import AppError from "../errors/appError";

export default async function (
  req: Request,
  res: Response,
  next: NextFunction
) {
  const userId = req.user.id;

  const user = await listExpecificUser(userId, res);

  if (!user?.isAdm) {
    throw new AppError("Unauthorized", 401);
  }

  return next();
}
