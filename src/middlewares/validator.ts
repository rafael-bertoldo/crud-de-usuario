import { Request, Response, NextFunction } from "express";
import { listExpecificUser } from "../controllers/user.controller";
import AppError from "../errors/appError";
import authConfig from "../config/auth";
import { verify } from "jsonwebtoken";

export default async function (
  req: Request,
  res: Response,
  next: NextFunction
) {
  const { isAdm } = req.body;

  if (isAdm) {
    throw new AppError("isAdm key cannot be changed", 401);
  }

  const userId = req.user.id;
  const { uuid } = req.params;

  const user = await listExpecificUser(userId, res);

  if (!user?.isAdm && userId !== uuid) {
    throw new AppError("Missing admin permissions", 401);
  }

  return next();
}
