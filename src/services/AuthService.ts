import { getCustomRepository } from "typeorm";
import User from "../entities/user";
import AppError from "../errors/appError";
import UserRepositorie from "../repositories/UserRepositorie";
import { compare } from "bcryptjs";
import authConfig from "../config/auth";
import { sign } from "jsonwebtoken";

interface Request {
  email: string;
  password: string;
}

interface Response {
  token: string;
}

export default class AuthService {
  public async execute({
    email,
    password,
  }: Request): Promise<Response | undefined> {
    const userRepository = getCustomRepository(UserRepositorie);

    const user = await userRepository.findByEmail(email);

    if (!user) {
      throw new AppError("Wrong email/password", 401);
    }

    const passwordMatch = await compare(password, user.password);

    if (!passwordMatch) {
      throw new AppError("Wrong email/password", 401);
    }

    const { expiresIn, secret } = authConfig.jwt;
    const token = sign({}, secret, {
      subject: user.uuid,
      expiresIn,
    });

    return {
      token,
    };
  }
}
