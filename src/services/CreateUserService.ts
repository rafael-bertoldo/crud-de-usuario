import { getCustomRepository } from "typeorm";
import UserRepositorie from "../repositories/UserRepositorie";
import User from "../entities/user";
import { hash } from "bcryptjs";
import AppError from "../errors/appError";

interface Request {
  name: string;
  email: string;
  password: string;
  isAdm: boolean;
}

interface Response {
  uuid: string;
  createdOn: Date;
  updatedOn: Date;
  name: string;
  email: string;
  isAdm: boolean;
}

export default class CreateUserService {
  public async execute({
    name,
    email,
    isAdm,
    password,
  }: Request): Promise<Response> {
    const userRepository = getCustomRepository(UserRepositorie);
    const checkUserExists = await userRepository.findByEmail(email);

    if (checkUserExists) {
      throw new AppError("E-mail already registered", 400);
    }

    const hashedPassword = await hash(password, 8);

    const user = userRepository.create({
      name,
      email,
      password: hashedPassword,
      isAdm,
    });

    await userRepository.save(user);

    const returnableUser = {
      uuid: user.uuid,
      createdOn: user.createdOn,
      updatedOn: user.updatedOn,
      name: user.name,
      email: user.email,
      isAdm: user.isAdm,
    };

    return returnableUser;
  }
}
