import { DeleteResult, getCustomRepository } from "typeorm";
import AppError from "../errors/appError";
import UserRepositorie from "../repositories/UserRepositorie";

interface Request {
  uuid: string;
}

export default class DeleteUserService {
  public async execute({ uuid }: Request): Promise<DeleteResult> {
    const userRepository = getCustomRepository(UserRepositorie);

    const user = await userRepository.findOne({
      where: {
        uuid,
      },
    });

    if (!user) {
      throw new AppError("User not found");
    }

    return userRepository.delete(uuid);
  }
}
