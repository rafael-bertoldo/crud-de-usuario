import { getCustomRepository } from "typeorm";
import User from "../entities/user";
import UserRepositorie from "../repositories/UserRepositorie";
import AppError from "../errors/appError";

interface Request {
  uuid: string;
  name: string;
  email: string;
}

export default class UpdateUserService {
  public async execute({ uuid, email, name }: Request): Promise<User> {
    const userRepository = getCustomRepository(UserRepositorie);

    const user = await userRepository.findOne({
      where: {
        uuid: uuid,
      },
    });

    if (!user) {
      throw new AppError("user not found");
    }

    const findExistEmail = await userRepository.findByEmail(email);

    if (findExistEmail && findExistEmail.uuid !== uuid) {
      throw new AppError("email already in use");
    }

    name ? (user.name = name) : user.name;
    email ? (user.email = email) : user.email;

    await userRepository.save(user);

    return user;
  }
}
