import CreateUserService from "../services/CreateUserService";
import { Response } from "express";
import { getCustomRepository } from "typeorm";
import UserRepositorie from "../repositories/UserRepositorie";
import { classToClass } from "class-transformer";
import UpdateUserService from "../services/UpdateUserService";
import DeleteUserService from "../services/DeleteUserService";

export const createUser = (
  name: string,
  email: string,
  password: string,
  isAdm: boolean,
  res: Response
) => {
  const createUser = new CreateUserService();
  const user = createUser.execute({
    email,
    isAdm,
    name,
    password,
  });

  return user;
};

export const listUser = (res: Response) => {
  const userRepository = getCustomRepository(UserRepositorie);

  const users = userRepository.find();

  return users;
};

export const listExpecificUser = (userId: string, res: Response) => {
  const userRepository = getCustomRepository(UserRepositorie);

  const user = userRepository.findOne(userId);

  return user;
};

export const updateUser = (name: string, email: string, uuid: string) => {
  const updatedUser = new UpdateUserService();

  const user = updatedUser.execute({
    name,
    email,
    uuid,
  });

  return user;
};

export const deleteUser = (uuid: string) => {
  const deletedUser = new DeleteUserService();

  const user = deletedUser.execute({
    uuid: uuid,
  });

  return user;
};
